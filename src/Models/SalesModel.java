package Models;

public class SalesModel {

    String ID_Client;
    String Date_Exam;
    String Medication;
    String Observation;
    String ID;
    String Code_Prod;
    String Brand;
    String Color;

    public SalesModel(String ID,String ID_Client, String Date_Exam, String Medication, String Observation, String Code_Prod, String Brand, String Color) {
        this.ID_Client = ID_Client;
        this.ID = ID;
        this.Date_Exam = Date_Exam;
        this.Medication = Medication;
        this.Observation = Observation;
        this.Code_Prod = Code_Prod;
        this.Brand = Brand;
        this.Color = Color;
    }

    public SalesModel() {
        this.ID_Client = "";
        this.ID = "";
        this.Date_Exam = "";
        this.Medication = "";
        this.Observation = "";
        this.Code_Prod = "";
        this.Brand = "";
        this.Color = "";
    }

    public String getID_Client() {
        return ID_Client;
    }

    public void setID_Client(String ID_Client) {
        this.ID_Client = ID_Client;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    
    public String getDate_Exam() {
        return Date_Exam;
    }

    public void setDate_Exam(String Date_Exam) {
        this.Date_Exam = Date_Exam;
    }

    public String getMedication() {
        return Medication;
    }

    public void setMedication(String Medication) {
        this.Medication = Medication;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String Observation) {
        this.Observation = Observation;
    }

    public String getCode_Prod() {
        return Code_Prod;
    }

    public void setCode_Prod(String Code_Prod) {
        this.Code_Prod = Code_Prod;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }
}