/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package Models.TableModel;

import Models.Stock.Stock;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author William Sanchez
 */
public class StockTableModel extends AbstractTableModel {

    private String[] columnNames = {"ID",
        "Brand",
        "Code",
        "Color",
        "Quantity",
        "Price"};
    private Object rowData[][];

    public StockTableModel() {

    }

    public StockTableModel(List<Stock> stocks) {
        rowData = new Object[stocks.size()][columnNames.length];
        int c = 0;
        for(Stock stock:stocks){
            rowData[c] = new Object[]{
                stock.getId(),
                stock.getBrand(),
                stock.getCode(),
                stock.getColor(),
                stock.getQuantity(),
                stock.getPrice()
            };
            c++;
        }
    }

    public void setDataModel(Object[][] data) {
        rowData = data;
    }

    public TableModel getModel() {
        TableModel model = new DefaultTableModel(
                rowData,
                columnNames
        );
        return model;
    }

    @Override
    public int getRowCount() {
        return rowData.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }

    public void saveTableData() {

    }
}
