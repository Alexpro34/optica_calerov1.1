/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controllers.SalesController;
import Models.SalesModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author pc
 */
public class Create_sales extends javax.swing.JFrame {

    /**
     * Creates new form Create_inventory
     */
    public Create_sales() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
        this.searchidButton.setCursor(new Cursor(HAND_CURSOR));
        this.searchcodeButton.setCursor(new Cursor(HAND_CURSOR));
        this.cleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.generatesalesButton.setCursor(new Cursor(HAND_CURSOR));
        setIconImage(new ImageIcon(getClass().getResource("/Resources/image/icono_barra.png")).getImage());
        setController();
    }

    public void setController() {
        SalesController sc = new SalesController(this);
        searchidButton.addActionListener(sc);
        searchcodeButton.addActionListener(sc);
        generatesalesButton.addActionListener(sc);
    }

    public SalesModel getSalesData() {
        SalesModel sm = new SalesModel();

        sm.setID_Client(idclientTextField.getText());
        sm.setDate_Exam(examdateTextField.getText());
        sm.setMedication(medicationTextField.getText());
        sm.setObservation(observationTextArea.getText());
        sm.setCode_Prod(codeTextField.getText());
        sm.setBrand(brandTextField.getText());
        sm.setColor(colorTextField.getText());

        return sm;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchcodeButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        observationTextArea = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        searchidButton = new javax.swing.JButton();
        cleanButton = new javax.swing.JButton();
        medicationTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        examdateTextField = new javax.swing.JTextField();
        idclientTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        codeTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        colorTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        brandTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        generatesalesButton = new javax.swing.JButton();
        cerrarButton = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        searchcodeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/codeButton.png"))); // NOI18N
        searchcodeButton.setActionCommand("SearchCode");
        searchcodeButton.setBorderPainted(false);
        searchcodeButton.setContentAreaFilled(false);
        searchcodeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchcodeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchcodeButtonMouseExited(evt);
            }
        });
        getContentPane().add(searchcodeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 70, 80, 30));

        observationTextArea.setBackground(new java.awt.Color(102, 106, 209));
        observationTextArea.setColumns(20);
        observationTextArea.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        observationTextArea.setForeground(new java.awt.Color(255, 255, 255));
        observationTextArea.setRows(5);
        jScrollPane1.setViewportView(observationTextArea);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 220, 500, 80));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 25, 112));
        jLabel15.setText("OBSERVATION");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 250, -1, -1));

        searchidButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/idButton.png"))); // NOI18N
        searchidButton.setActionCommand("SearchID");
        searchidButton.setAutoscrolls(true);
        searchidButton.setBorderPainted(false);
        searchidButton.setContentAreaFilled(false);
        searchidButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchidButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchidButtonMouseExited(evt);
            }
        });
        searchidButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchidButtonActionPerformed(evt);
            }
        });
        getContentPane().add(searchidButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, 60, 30));

        cleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton.png"))); // NOI18N
        cleanButton.setBorderPainted(false);
        cleanButton.setContentAreaFilled(false);
        cleanButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cleanButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cleanButtonMouseExited(evt);
            }
        });
        getContentPane().add(cleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 320, 80, 30));

        medicationTextField.setBackground(new java.awt.Color(102, 106, 209));
        medicationTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        medicationTextField.setForeground(new java.awt.Color(255, 255, 255));
        medicationTextField.setBorder(null);
        getContentPane().add(medicationTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 130, 30));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 170, 160, 30));

        examdateTextField.setBackground(new java.awt.Color(102, 106, 209));
        examdateTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        examdateTextField.setForeground(new java.awt.Color(255, 255, 255));
        examdateTextField.setBorder(null);
        getContentPane().add(examdateTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 120, 130, 30));

        idclientTextField.setBackground(new java.awt.Color(102, 106, 209));
        idclientTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        idclientTextField.setForeground(new java.awt.Color(255, 255, 255));
        idclientTextField.setBorder(null);
        getContentPane().add(idclientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 70, 80, 30));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, 160, 30));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text_small.png"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, 100, 30));

        jLabel7.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 25, 112));
        jLabel7.setText("COLOR");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, -1, -1));

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 25, 112));
        jLabel5.setText("DATE EXAM");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, -1, -1));

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 25, 112));
        jLabel4.setText("MEDICATION");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 180, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 25, 112));
        jLabel3.setText("CODE");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, -1, 30));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 25, 112));
        jLabel2.setText("ID CLIENT");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, 30));

        jLabel6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 25, 112));
        jLabel6.setText("BRAND");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 130, -1, -1));

        codeTextField.setBackground(new java.awt.Color(102, 106, 209));
        codeTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        codeTextField.setForeground(new java.awt.Color(255, 255, 255));
        codeTextField.setBorder(null);
        getContentPane().add(codeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, 80, 30));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text_small.png"))); // NOI18N
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 70, 100, 30));

        colorTextField.setBackground(new java.awt.Color(102, 106, 209));
        colorTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        colorTextField.setForeground(new java.awt.Color(255, 255, 255));
        colorTextField.setBorder(null);
        getContentPane().add(colorTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 170, 130, 30));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 170, 160, 30));

        brandTextField.setBackground(new java.awt.Color(102, 106, 209));
        brandTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        brandTextField.setForeground(new java.awt.Color(255, 255, 255));
        brandTextField.setBorder(null);
        getContentPane().add(brandTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 120, 130, 30));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 120, 160, 30));

        generatesalesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/generatesalesButton.png"))); // NOI18N
        generatesalesButton.setActionCommand("GenerateSales");
        generatesalesButton.setBorderPainted(false);
        generatesalesButton.setContentAreaFilled(false);
        generatesalesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseExited(evt);
            }
        });
        getContentPane().add(generatesalesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 320, 160, 30));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit.png"))); // NOI18N
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        cerrarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cerrarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cerrarButtonMouseExited(evt);
            }
        });
        cerrarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarButtonActionPerformed(evt);
            }
        });
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 10, 50, 30));

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/image/fondo_createsales.png"))); // NOI18N
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 380));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cerrarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarButtonMouseEntered
        cerrarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit_efecto.png")));
    }//GEN-LAST:event_cerrarButtonMouseEntered

    private void cerrarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarButtonMouseExited
        cerrarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit.png")));
    }//GEN-LAST:event_cerrarButtonMouseExited

    private void generatesalesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseEntered
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/generatesalesButton_efecto.png")));
    }//GEN-LAST:event_generatesalesButtonMouseEntered

    private void generatesalesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseExited
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/generatesalesButton.png")));
    }//GEN-LAST:event_generatesalesButtonMouseExited

    private void cerrarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cerrarButtonActionPerformed

    private void cleanButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cleanButtonMouseEntered
        cleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton_efecto.png")));
    }//GEN-LAST:event_cleanButtonMouseEntered

    private void cleanButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cleanButtonMouseExited
        cleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton.png")));
    }//GEN-LAST:event_cleanButtonMouseExited

    private void searchidButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchidButtonMouseEntered
        searchidButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/idButton_efecto.png")));
    }//GEN-LAST:event_searchidButtonMouseEntered

    private void searchidButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchidButtonMouseExited
        searchidButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/idButton.png")));
    }//GEN-LAST:event_searchidButtonMouseExited

    private void searchcodeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchcodeButtonMouseEntered
        searchcodeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/codeButton_efecto.png")));
    }//GEN-LAST:event_searchcodeButtonMouseEntered

    private void searchcodeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchcodeButtonMouseExited
        searchcodeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/codeButton.png")));
    }//GEN-LAST:event_searchcodeButtonMouseExited

    private void searchidButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchidButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchidButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Create_sales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Create_sales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Create_sales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Create_sales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Create_sales().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField brandTextField;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JButton cleanButton;
    private javax.swing.JTextField codeTextField;
    public javax.swing.JTextField colorTextField;
    public javax.swing.JTextField examdateTextField;
    private javax.swing.JButton generatesalesButton;
    private javax.swing.JTextField idclientTextField;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextField medicationTextField;
    public javax.swing.JTextArea observationTextArea;
    private javax.swing.JButton searchcodeButton;
    private javax.swing.JButton searchidButton;
    // End of variables declaration//GEN-END:variables
}
