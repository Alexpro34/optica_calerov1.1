/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Panels.CambiaPanel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 *
 * @author pc
 */
public class EmployeePrincipalForm extends javax.swing.JFrame {

    /**
     * Creates new form PrincipalForm
     */
    public EmployeePrincipalForm() {
        initComponents();
        this.homeButton.setCursor(new Cursor(HAND_CURSOR));
        this.salesButton.setCursor(new Cursor(HAND_CURSOR));
        this.optometryButton.setCursor(new Cursor(HAND_CURSOR));
        this.clientButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrar_sesionButton.setCursor(new Cursor(HAND_CURSOR));
        new CambiaPanel(Panelprincipal, new Panels.homePanel());
        setIconImage(new ImageIcon(getClass().getResource("/Resources/image/icono_barra.png")).getImage());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        cerrar_sesionButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        clientButton = new javax.swing.JButton();
        homeButton = new javax.swing.JButton();
        salesButton = new javax.swing.JButton();
        optometryButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        Panelprincipal = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Employee");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 27, 120, 20));

        cerrar_sesionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sesion.png"))); // NOI18N
        cerrar_sesionButton.setBorderPainted(false);
        cerrar_sesionButton.setContentAreaFilled(false);
        cerrar_sesionButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cerrar_sesionButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cerrar_sesionButtonMouseExited(evt);
            }
        });
        cerrar_sesionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrar_sesionButtonActionPerformed(evt);
            }
        });
        getContentPane().add(cerrar_sesionButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1310, 0, 50, 50));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Óptica Calero");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 0, -1, 30));

        clientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/register_client.png"))); // NOI18N
        clientButton.setBorderPainted(false);
        clientButton.setContentAreaFilled(false);
        clientButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                clientButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                clientButtonMouseExited(evt);
            }
        });
        clientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientButtonActionPerformed(evt);
            }
        });
        getContentPane().add(clientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 210, 50));

        homeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/home.png"))); // NOI18N
        homeButton.setBorderPainted(false);
        homeButton.setContentAreaFilled(false);
        homeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                homeButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                homeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                homeButtonMouseExited(evt);
            }
        });
        homeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeButtonActionPerformed(evt);
            }
        });
        getContentPane().add(homeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 0, 130, 50));

        salesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sales.png"))); // NOI18N
        salesButton.setBorderPainted(false);
        salesButton.setContentAreaFilled(false);
        salesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                salesButtonMouseExited(evt);
            }
        });
        salesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesButtonActionPerformed(evt);
            }
        });
        getContentPane().add(salesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 170, 50));

        optometryButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/optometry.png"))); // NOI18N
        optometryButton.setBorderPainted(false);
        optometryButton.setContentAreaFilled(false);
        optometryButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                optometryButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                optometryButtonMouseExited(evt);
            }
        });
        optometryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optometryButtonActionPerformed(evt);
            }
        });
        getContentPane().add(optometryButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 0, 170, 50));

        jLabel4.setBackground(new java.awt.Color(0, 25, 112));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/image/barra.png"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1366, 50));

        Panelprincipal.setBackground(new java.awt.Color(255, 255, 255));
        Panelprincipal.setLayout(new javax.swing.BoxLayout(Panelprincipal, javax.swing.BoxLayout.LINE_AXIS));
        getContentPane().add(Panelprincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 1366, 692));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void homeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeButtonMouseEntered
         homeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/home_efecto.png")));
    }//GEN-LAST:event_homeButtonMouseEntered

    private void homeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeButtonMouseExited
        homeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/home.png")));
    }//GEN-LAST:event_homeButtonMouseExited

    private void salesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesButtonMouseEntered
        salesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sales_efecto.png")));
    }//GEN-LAST:event_salesButtonMouseEntered

    private void salesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesButtonMouseExited
        salesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sales.png")));
    }//GEN-LAST:event_salesButtonMouseExited

    private void optometryButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_optometryButtonMouseEntered
         optometryButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/optometry_efecto.png")));
    }//GEN-LAST:event_optometryButtonMouseEntered

    private void optometryButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_optometryButtonMouseExited
         optometryButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/optometry.png")));

    }//GEN-LAST:event_optometryButtonMouseExited

    private void cerrar_sesionButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrar_sesionButtonMouseEntered
        cerrar_sesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sesion_efecto.png")));
    }//GEN-LAST:event_cerrar_sesionButtonMouseEntered

    private void cerrar_sesionButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrar_sesionButtonMouseExited
        cerrar_sesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/sesion.png")));
    }//GEN-LAST:event_cerrar_sesionButtonMouseExited

    private void homeButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeButtonMouseClicked
        if (homeButton.isSelected()) {
            //CAPTURA LO ESCRITO Y LO MUESTA TIPO STRING
            homeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/home_clicked.png")));  
        } else {
            //CAPTURA LO ESCRITO Y LO MUESTR CON 
            homeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/home.png")));  
        }
        
    }//GEN-LAST:event_homeButtonMouseClicked

    private void cerrar_sesionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrar_sesionButtonActionPerformed
        LoginForm L = new LoginForm();
        L.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_cerrar_sesionButtonActionPerformed

    private void homeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeButtonActionPerformed
        new CambiaPanel(Panelprincipal, new Panels.homePanel());
        
    }//GEN-LAST:event_homeButtonActionPerformed

    private void salesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesButtonActionPerformed
       new CambiaPanel(Panelprincipal, new Panels.salesPanel());
    }//GEN-LAST:event_salesButtonActionPerformed

    private void optometryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optometryButtonActionPerformed
        new CambiaPanel(Panelprincipal, new Panels.optometryPanel());
    }//GEN-LAST:event_optometryButtonActionPerformed

    private void clientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientButtonActionPerformed
        new CambiaPanel(Panelprincipal, new Panels.clientRegistrationPanel());
    }//GEN-LAST:event_clientButtonActionPerformed

    private void clientButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientButtonMouseExited
        clientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/register_client.png")));
    }//GEN-LAST:event_clientButtonMouseExited

    private void clientButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientButtonMouseEntered
        clientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/register_client_efecto.png")));
    }//GEN-LAST:event_clientButtonMouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployeePrincipalForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmployeePrincipalForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmployeePrincipalForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmployeePrincipalForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EmployeePrincipalForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panelprincipal;
    private javax.swing.JButton cerrar_sesionButton;
    private javax.swing.JButton clientButton;
    private javax.swing.JButton homeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton optometryButton;
    private javax.swing.JButton salesButton;
    // End of variables declaration//GEN-END:variables
}
