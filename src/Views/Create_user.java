/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import ConnectionSql.ConnectionSQLServer;
import Controllers.UserController;
import Models.UserModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author pc
 */
public class Create_user extends javax.swing.JFrame {

    /**
     * Creates new form Create_inventory
     */
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    UserController UC = new UserController(this);

    public Create_user(int id) {
        initComponents();
        this.setLocationRelativeTo(this);
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.saveButton.setCursor(new Cursor(HAND_CURSOR));
        setIconImage(new ImageIcon(getClass().getResource("/Resources/image/icono_barra.png")).getImage());
        showItems();
        setData(UC.getUser(id));
        setController();
    }

    public Create_user() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.saveButton.setCursor(new Cursor(HAND_CURSOR));
        setIconImage(new ImageIcon(getClass().getResource("/Resources/image/icono_barra.png")).getImage());
        showItems();
        setController();
    }

    public void setController(){
        saveButton.addActionListener(UC);
        
    }
    public void Clean() {
        IDUserTextField.setText("");
        IDCedTextField.setText("");
        NameTextField.setText("");
        LastNameTextField.setText("");
        UserTextField.setText("");
        PasswordTextField.setText("");
        RoleTextField.setSelectedIndex(0);
        DirectionTextField.setText("");
        PhoneTextField.setText("");
        EmailTextField.setText("");
        Departments.setSelectedItem(-1);
        Municipality.setSelectedItem(-1);
    }

    public void setData(UserModel u) {
        IDUserTextField.setText(String.valueOf(u.getID_User()));
        IDCedTextField.setText(u.getID_Ced());
        NameTextField.setText(u.getName());
        LastNameTextField.setText(u.getLastName());
        UserTextField.setText(u.getUser());
        PasswordTextField.setText(u.getPassword());
        RoleTextField.setSelectedItem(u.getRole());
        DirectionTextField.setText(u.getDirection());
        PhoneTextField.setText(u.getPhone());
        EmailTextField.setText(u.getMail());
        Departments.setSelectedItem(u.getDepartment());
        Municipality.setSelectedItem(u.getMunicipality());
    }
    
    public UserModel getData(){
        UserModel u = new UserModel();
        
        u.setID_User(Integer.parseInt(IDUserTextField.getText()));
        u.setID_Ced(IDCedTextField.getText());
        u.setUser(UserTextField.getText());
        u.setPassword(String.valueOf(PasswordTextField.getPassword()));
        u.setName(NameTextField.getText());
        u.setLastName(LastNameTextField.getText());
        u.setPhone(PhoneTextField.getText());
        u.setMail(EmailTextField.getText());
        u.setDepartment(Departments.getSelectedItem().toString());
        u.setMunicipality(Municipality.getSelectedItem().toString());
        u.setRole(RoleTextField.getSelectedItem().toString());
        u.setDirection(DirectionTextField.getText());
        
        return u;
    }
    
    public void showItems(String D) {
        int i;
        Departments.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        lista = fullD(D);
        for (i = 0; i < lista.size(); i++) {
            Departments.addItem(lista.get(i));
        }

    }

    public void showItems() {
        int i;
        Departments.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        lista = fullD();
        for (i = 0; i < lista.size(); i++) {
            Departments.addItem(lista.get(i));
        }

    }
    
    public ArrayList<String> fullD(String D) {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT * FROM dbo.departments Where Name_Departments = ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);
            
            ps.setString(1, D);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Departments"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    public ArrayList<String> fullD() {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT * FROM dbo.departments";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Departments"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    public ArrayList<String> fullM(String name) {
        ArrayList<String> Lista = new ArrayList<String>();
        try {
            String q = "SELECT m.Name_Municipality FROM dbo.departments d\n"
                    + "INNER JOIN dbo.Municipality M ON M.ID = d.ID\n"
                    + "WHERE d.Name_Departments = ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(q);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lista.add(rs.getString("Name_Municipality"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return Lista;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Departments = new javax.swing.JComboBox<>();
        Municipality = new javax.swing.JComboBox<>();
        jLabel26 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        EmailTextField = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        PhoneTextField = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        DirectionTextField = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        NameTextField = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        PasswordTextField = new javax.swing.JPasswordField();
        jLabel16 = new javax.swing.JLabel();
        cleanButton = new javax.swing.JButton();
        UserTextField = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        IDCedTextField = new javax.swing.JTextField();
        IDUserTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        LastNameTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        saveButton = new javax.swing.JButton();
        cerrarButton = new javax.swing.JButton();
        RoleTextField = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Departments.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        Departments.setForeground(new java.awt.Color(0, 25, 112));
        Departments.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        Departments.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DepartmentsMouseClicked(evt);
            }
        });
        Departments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepartmentsActionPerformed(evt);
            }
        });
        Departments.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DepartmentsKeyReleased(evt);
            }
        });
        getContentPane().add(Departments, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 140, 150, 30));

        Municipality.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        Municipality.setForeground(new java.awt.Color(0, 25, 112));
        Municipality.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        Municipality.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MunicipalityActionPerformed(evt);
            }
        });
        getContentPane().add(Municipality, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 190, 150, 30));

        jLabel26.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(0, 25, 112));
        jLabel26.setText("MUNICIPALITY");
        getContentPane().add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 190, -1, 30));

        jLabel24.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(0, 25, 112));
        jLabel24.setText("DEPARTAMENT");
        getContentPane().add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 140, -1, 30));

        EmailTextField.setBackground(new java.awt.Color(102, 106, 209));
        EmailTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        EmailTextField.setForeground(new java.awt.Color(255, 255, 255));
        EmailTextField.setBorder(null);
        getContentPane().add(EmailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 90, 130, 30));

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 90, 160, 30));

        jLabel23.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 25, 112));
        jLabel23.setText("EMAIL");
        getContentPane().add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 90, -1, 30));

        PhoneTextField.setBackground(new java.awt.Color(102, 106, 209));
        PhoneTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        PhoneTextField.setForeground(new java.awt.Color(255, 255, 255));
        PhoneTextField.setBorder(null);
        getContentPane().add(PhoneTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 40, 130, 30));

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 40, 160, 30));

        jLabel21.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(0, 25, 112));
        jLabel21.setText("PHONE");
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 40, -1, 30));

        DirectionTextField.setBackground(new java.awt.Color(102, 106, 209));
        DirectionTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        DirectionTextField.setForeground(new java.awt.Color(255, 255, 255));
        DirectionTextField.setBorder(null);
        getContentPane().add(DirectionTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 190, 130, 30));

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 190, 160, 30));

        jLabel19.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(0, 25, 112));
        jLabel19.setText("DIRECTION");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, -1, -1));

        NameTextField.setBackground(new java.awt.Color(102, 106, 209));
        NameTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        NameTextField.setForeground(new java.awt.Color(255, 255, 255));
        NameTextField.setBorder(null);
        getContentPane().add(NameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 40, 130, 30));

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 40, 160, 30));

        PasswordTextField.setBackground(new java.awt.Color(102, 106, 209));
        PasswordTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        PasswordTextField.setForeground(new java.awt.Color(255, 255, 255));
        PasswordTextField.setBorder(null);
        getContentPane().add(PasswordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(132, 190, 130, 30));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 190, 160, 30));

        cleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton.png"))); // NOI18N
        cleanButton.setBorderPainted(false);
        cleanButton.setContentAreaFilled(false);
        cleanButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cleanButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cleanButtonMouseExited(evt);
            }
        });
        getContentPane().add(cleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 270, 80, 30));

        UserTextField.setBackground(new java.awt.Color(102, 106, 209));
        UserTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        UserTextField.setForeground(new java.awt.Color(255, 255, 255));
        UserTextField.setBorder(null);
        getContentPane().add(UserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 140, 130, 30));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 25, 112));
        jLabel15.setText("PASSWORD");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, -1, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 140, 160, 30));

        IDCedTextField.setBackground(new java.awt.Color(102, 106, 209));
        IDCedTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        IDCedTextField.setForeground(new java.awt.Color(255, 255, 255));
        IDCedTextField.setBorder(null);
        getContentPane().add(IDCedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 130, 30));

        IDUserTextField.setBackground(new java.awt.Color(102, 106, 209));
        IDUserTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        IDUserTextField.setForeground(new java.awt.Color(255, 255, 255));
        IDUserTextField.setText("0");
        IDUserTextField.setBorder(null);
        getContentPane().add(IDUserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 40, 80, 30));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 160, 30));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text_small.png"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 100, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text_small.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 100, 30));

        jLabel7.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 25, 112));
        jLabel7.setText("ROLE");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 150, -1, -1));

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 25, 112));
        jLabel5.setText("ID_CED");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 25, 112));
        jLabel4.setText("USER");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 25, 112));
        jLabel3.setText("NAME");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, -1, 30));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 25, 112));
        jLabel2.setText("ID_USER");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, 30));

        jLabel6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 25, 112));
        jLabel6.setText("LAST NAME");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, -1, -1));

        LastNameTextField.setBackground(new java.awt.Color(102, 106, 209));
        LastNameTextField.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        LastNameTextField.setForeground(new java.awt.Color(255, 255, 255));
        LastNameTextField.setBorder(null);
        getContentPane().add(LastNameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 90, 130, 30));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/text.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 160, 30));

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/saveButton.png"))); // NOI18N
        saveButton.setActionCommand("Save");
        saveButton.setBorderPainted(false);
        saveButton.setContentAreaFilled(false);
        saveButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                saveButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                saveButtonMouseExited(evt);
            }
        });
        getContentPane().add(saveButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 270, 110, 30));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit.png"))); // NOI18N
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        cerrarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cerrarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cerrarButtonMouseExited(evt);
            }
        });
        cerrarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarButtonActionPerformed(evt);
            }
        });
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 10, 30, 30));

        RoleTextField.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Admin", "Employee" }));
        getContentPane().add(RoleTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 146, 150, 30));

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/image/fondo_createuser.png"))); // NOI18N
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 870, 340));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cerrarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarButtonMouseEntered
        cerrarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit_efecto.png")));
    }//GEN-LAST:event_cerrarButtonMouseEntered

    private void cerrarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarButtonMouseExited
        cerrarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/button/exit.png")));
    }//GEN-LAST:event_cerrarButtonMouseExited

    private void saveButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveButtonMouseEntered
        saveButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/saveButton_efecto.png")));
    }//GEN-LAST:event_saveButtonMouseEntered

    private void saveButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveButtonMouseExited
        saveButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/saveButton.png")));
    }//GEN-LAST:event_saveButtonMouseExited

    private void cerrarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cerrarButtonActionPerformed

    private void cleanButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cleanButtonMouseEntered
        cleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton_efecto.png")));
    }//GEN-LAST:event_cleanButtonMouseEntered

    private void cleanButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cleanButtonMouseExited
        cleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Resources/Buttons/cleanButton.png")));
    }//GEN-LAST:event_cleanButtonMouseExited

    private void DepartmentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepartmentsActionPerformed
        Municipality.removeAllItems();
        Departments.addItem("");
        int i;
        Municipality.removeAllItems();
        ArrayList<String> lista = new ArrayList<String>();
        String nombre = Departments.getSelectedItem().toString();
        lista = fullM(nombre);
        for (i = 0; i < lista.size(); i++) {
            Municipality.addItem(lista.get(i));
        }
    }//GEN-LAST:event_DepartmentsActionPerformed

    private void MunicipalityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MunicipalityActionPerformed

    }//GEN-LAST:event_MunicipalityActionPerformed

    private void DepartmentsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DepartmentsMouseClicked

    }//GEN-LAST:event_DepartmentsMouseClicked

    private void DepartmentsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DepartmentsKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_DepartmentsKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Create_user.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Create_user.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Create_user.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Create_user.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Create_user().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Departments;
    private javax.swing.JTextField DirectionTextField;
    private javax.swing.JTextField EmailTextField;
    private javax.swing.JTextField IDCedTextField;
    private javax.swing.JTextField IDUserTextField;
    private javax.swing.JTextField LastNameTextField;
    private javax.swing.JComboBox<String> Municipality;
    private javax.swing.JTextField NameTextField;
    private javax.swing.JPasswordField PasswordTextField;
    private javax.swing.JTextField PhoneTextField;
    private javax.swing.JComboBox<String> RoleTextField;
    private javax.swing.JTextField UserTextField;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JButton cleanButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables
}
