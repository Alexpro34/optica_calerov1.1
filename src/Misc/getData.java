/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;

/**
 *
 * @author Paulino Zelaya
 */
public class getData {

    String Nombre_PC;
    String Publica;
    String Local;

    public String ObtenerIP_Publica() throws MalformedURLException, IOException {

        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = new BufferedReader(new InputStreamReader(
                whatismyip.openStream()));

        Publica = in.readLine();
        return Publica;
    }

    public String ObtenerIP_Local() {
        try {
            InetAddress thisIp = InetAddress.getLocalHost();
            Local = thisIp.getHostAddress().toString();
        } catch (Exception e) {
        }
        return Local;

    }

    public String Obtener_Nombre() throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();

        Nombre_PC = address.getHostName();
        return Nombre_PC;
    }

    public String getPublica() {
        return Publica;
    }

    public String getLocal() {
        return Local;
    }

    public String getNombre() {
        return Nombre_PC;
    }
}
