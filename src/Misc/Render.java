package Misc;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

public class Render extends DefaultTableCellRenderer{

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, 
            boolean isSelected, boolean hasFocus, int row, int column) {       
        
        if (evaluarpar(row)) {
            this.setBackground(Color.gray);
            this.setForeground(Color.black);
        } else {
            this.setBackground(Color.white);
            this.setForeground(Color.black);
        }
        
        return super.getTableCellRendererComponent(table, value, isSelected, 
                hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean evaluarpar(int numero) {
        return (numero % 2 == 0);
    }
    
}
