package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Models.OptometryModel;
import Panels.optometryPanel;
import Views.Create_patient;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class OptometryControllers implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    Create_patient CP;
    OptometryModel OM;
    optometryPanel OP;

    public OptometryControllers(Create_patient CP) {
        super();
        this.CP = CP;
        OM = new OptometryModel();
    }

    public OptometryControllers(optometryPanel OP) {
        super();
        this.OP = OP;
        OM = new OptometryModel();
    }

    public OptometryModel getOM() {
        return OM;
    }

    public void setOM(OptometryModel OM) {
        this.OM = OM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Save":
                OM = CP.getData();

                if (OM.getID_Exam() == 0) {
                    Insert();
                } else {
                    Update();
                }
                break;
            case "Modify":
                ShowCategory(OP.getSelectedCategory());
                break;

            case "Delete":
                Delete(OP.getSelectedCategory());
                break;
        }
    }

    public void Insert() {
        OM = CP.getData();
        
        String ID_Cliente = OM.getID_Cliente();
        //String Exam_Date = om.getExam_Date();
        String Eyes_Right = OM.getEyes_Right();
        String Eyes_Left = OM.getEyes_Left();
        String Distance = OM.getDistance();
        String Lenses_Type = OM.getLenses_Type();
        String Medication = OM.getMedication();
        String Observation = OM.getObservation();

        try {
            String sql = " DECLARE @n TINYINT,@nm VARCHAR(500)"
                    + " EXEC OptometryInsert ?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT "
                    + " SELECT @n Codigo,@nm Mensaje";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Cliente);
            ps.setString(2, Eyes_Right);
            ps.setString(3, Eyes_Left);
            ps.setString(4, Distance);
            ps.setString(5, Lenses_Type);
            ps.setString(6, Medication);
            ps.setString(7, Observation);

            rs = ps.executeQuery();

            if (ID_Cliente.isEmpty() || Eyes_Right.isEmpty() || Eyes_Left.isEmpty() || Distance.isEmpty() || Lenses_Type.isEmpty() || Medication.isEmpty()) {
                JOptionPane.showMessageDialog(null,"Porfavor rellene todos los campos para continuar");
                for (int i = 0; i < 1000; i++) {
                    
                }
            } else if (rs.next()) {
                int validar = rs.getInt("Codigo");

                if (validar == 0) {
                    JOptionPane.showMessageDialog(null,rs.getString("Mensaje"));
                   
                } else if (validar == 1) {
                    JOptionPane.showMessageDialog(null,rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void Update() {

    }

    public void Delete(int id) {

    }
    public void ShowCategory(int selectedCategory) {
    
    }

}
