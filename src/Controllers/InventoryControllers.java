package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Controllers.Stock.StockJpaController;
import Misc.ShowTables;
import Models.Stock.Stock;
import Models.TableModel.StockTableModel;
import Panels.inventoryPanel;
import Views.Create_inventory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.TableModel;

public class InventoryControllers implements ActionListener {

    Create_inventory CI;
    Stock IM;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    EntityManagerFactory emf;
    inventoryPanel IP;

    public InventoryControllers(Create_inventory CI) {
        super();
        this.CI = CI;
        emf = Persistence.createEntityManagerFactory("Optica_caleroV1.1PU");
        IM = new Stock();
    }

    public InventoryControllers(inventoryPanel IP) {
        super();
        this.IP = IP;
        emf = Persistence.createEntityManagerFactory("Optica_caleroV1.1PU");
        IM = new Stock();
    }

    public Stock getIM() {
        return IM;
    }

    public void setIM(Stock IM) {
        this.IM = IM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Save":
                IM = CI.getData();

                if (IM.getId() == 0) {
                    save(IM);
                } else {
                    update(IM);
                }
                break;
            case "Clean":
                CI.Clean();
                break;
            case "Modify":
                showCategoryForm(IP.getSelectedCategory());
                break;
            case "Delete":
                delete(IP.getSelectedCategory());
                break;
        }
    }

    public Stock getClient(int ID) {
        StockJpaController db = new StockJpaController(emf);
        Stock stocks;
        try {
            stocks = db.findStock(ID);

        } catch (Exception e) {
            stocks = new Stock();
        }

        return stocks;
    }

    private void showCategoryForm(int selectedCategory) {
        CI = selectedCategory == 0 ? new Create_inventory() : new Create_inventory(selectedCategory);
        //JDesktopPane desktop = (JDesktopPane) clientList.getParent();
        //desktop.add(clientFrame);
        CI.setVisible(true);
    }

    public void save(Stock IM) {
        StockJpaController db = new StockJpaController(emf);
        try {
            db.create(IM);
            JOptionPane.showMessageDialog(CI, "Ingresado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(CI, e.getMessage());
        }
    }

    public void update(Stock IM) {
        StockJpaController db = new StockJpaController(emf);
        try {
            db.edit(IM);
            JOptionPane.showMessageDialog(CI, "Actualizado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(CI, e.getMessage());
        }
    }

    public void delete(int id) {
        StockJpaController db = new StockJpaController(emf);
        try {
            db.destroy(id);
            JOptionPane.showMessageDialog(null, "Eliminado Con Exito");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public TableModel getCagories() {
        StockTableModel ctm;
        StockJpaController db = new StockJpaController(emf);
        try {
            List<Stock> stocks = db.findStockEntities();
            ctm = new StockTableModel(stocks);
        } catch (Exception e) {
            ctm = new StockTableModel();
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return ctm.getModel();
    }

}
