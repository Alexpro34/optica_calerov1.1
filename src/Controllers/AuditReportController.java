/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Panels.toolsPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Paulino Zelaya
 */
public class AuditReportController implements ActionListener {
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    toolsPanel tp;
    
    public AuditReportController(toolsPanel tp){
        super();
        this.tp = tp;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       switch(e.getActionCommand()){
           case "INVENTORY":
               stock();
               break;
           case "SALES":
               sales();
               break;
       }
    }
    
    public void stock(){
        try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reports/StockAudit.jasper";
                String Logo = System.getProperty("user.dir") + "/src/Resources/logoventana.png";
                String Icono = System.getProperty("user.dir") + "/src/Resources/icono.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                parameters.put("Icono",Icono);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte); 
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);                
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/Resources/logoventana.png")).getImage());
                view.setTitle("Sales Record");
                view.setVisible(true);          
            } catch (Exception e) {System.out.println(e.getMessage());}
    }
    
    public void sales(){
         try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reports/SalesAudit.jasper";
                String Logo = System.getProperty("user.dir") + "/src/Resources/logoventana.png";
                String Icono = System.getProperty("user.dir") + "/src/Resources/icono.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                parameters.put("Icono",Icono);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte); 
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);                
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/Resources/logoventana.png")).getImage());
                view.setTitle("Sales Record");
                view.setVisible(true);          
            } catch (Exception e) {System.out.println(e.getMessage());}
    }
    
}
