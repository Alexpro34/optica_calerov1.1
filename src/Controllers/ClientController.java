package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Models.ClientModel;
import Models.UserModel;
import Panels.clientRegistrationPanel;
import Panels.userRegistrationPanel;
import Views.Create_client;
import Views.Create_user;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class ClientController implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    clientRegistrationPanel CR;
    Create_client CC;
    ClientModel CM;

    public ClientController(clientRegistrationPanel CR) {
        super();
        this.CR = CR;
        CM = new ClientModel();
    }

    public ClientController(Create_client CC) {
        super();
        this.CC = CC;
        CM = new ClientModel();
    }

    public ClientModel getCM() {
        return CM;
    }

    public void setCM(ClientModel CM) {
        this.CM = CM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Save":
                CM = CC.getData();

                if (CM.getID() == 0) {
                    Insert();
                } else {
                    Update();
                }
                break;
            case "Modify":
                ShowCategory(CR.getSelectedCategory());
                break;

            case "Delete":
                Delete(CR.getSelectedCategory());
                break;
        }
    }

    public void Delete(int id) {
        try {

            String sql1 = "EXEC ClientSearchID ?";

            ResultSet rs1 = null;
            PreparedStatement ps1 = null;

            ps1 = cc.getConnection().prepareStatement(sql1);

            ps1.setInt(1, id);

            rs1 = ps1.executeQuery();

            if (rs1.next()) {
                String ID_Ced;

                ID_Ced = rs1.getString("Ced");
                String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                        + "EXEC ClientDelete ?,@n OUTPUT,@nm OUTPUT;"
                        + "select @n Codigo,@nm Mensaje";

                ResultSet rs = null;
                PreparedStatement ps = null;

                ps = cc.getConnection().prepareStatement(sql);

                ps.setString(1, ID_Ced);

                rs = ps.executeQuery();

                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, rs.getString("Mensaje"));
                }

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void Insert() {

        CM = CC.getData();

        try {
            int ID = CM.getID();
            String Ced = CM.getCed();
            String Name = CM.getName();
            String LastName = CM.getLastName();
            String Age = CM.getAge();
            String Direction = CM.getDirection();
            String Mail = CM.getMail();
            String Phone = CM.getPhone();

            Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
            String email = Mail;
            Matcher mather = pattern.matcher(email);

            if (Ced.length() == 14) {
                if (!Character.isLetter(Ced.charAt(13))) {
                    JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
                } else {
                    if (Name.isEmpty()) {
                        JOptionPane.showMessageDialog(null,"Debe Rellenar Todos Los Campos");
                    } else {
                        if (mather.find() == true) {

                            String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                                    + "EXEC ClientInsert ?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                                    + "select @n Codigo,@nm Mensaje";

                            ResultSet rs = null;
                            PreparedStatement ps = null;

                            ps = cc.getConnection().prepareStatement(sql);

                            ps.setString(1, Ced);
                            ps.setString(2, Name);
                            ps.setString(3, LastName);
                            ps.setString(4, Age);
                            ps.setString(5, Direction);
                            ps.setString(6, Mail);
                            ps.setString(7, Phone);

                            rs = ps.executeQuery();

                            if (rs.next()) {
                                JOptionPane.showMessageDialog(null,"Client Entered Correctly");
                            }
                            //Enviar();

                        } else {
                            JOptionPane.showMessageDialog(null,"El Mail ingresado debe ser valido");
                        }
                    }
                }

            } else {
                JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void Update() {
        CM = CC.getData();

        try {
            int ID_Client = CM.getID();
            String ID_Ced = CM.getCed();
            String Name = CM.getName();
            String LastName = CM.getLastName();
            String Age = CM.getAge();
            String Phone = CM.getPhone();
            String Mail = CM.getMail();
            String Direction = CM.getDirection();

            if (ID_Ced.length() == 14) {
                if (!Character.isLetter(ID_Ced.charAt(13))) {
                     JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
                } else {
                    String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                            + "EXEC ClientUpdate ?,?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                            + "select @n Codigo,@nm Mensaje";

                    ResultSet rs = null;
                    PreparedStatement ps = null;

                    ps = cc.getConnection().prepareStatement(sql);

                    ps.setInt(1, ID_Client);
                    ps.setString(2, ID_Ced);
                    ps.setString(3, Name);
                    ps.setString(4, LastName);
                    ps.setString(5, Age);
                    ps.setString(6, Direction);
                    ps.setString(7, Mail);
                    ps.setString(8, Phone);

                    rs = ps.executeQuery();
                    
                    if (rs.next()) {
                        String valida = rs.getString("Codigo");

                        if (valida.equals("0")) {
                             JOptionPane.showMessageDialog(null,"Client Modified Correctly");
                        } else if (valida.equals("1")) {
                             JOptionPane.showMessageDialog(null,"Client Already Exists");
                        }
                    }
                }
            } else {
                 JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public ClientModel getClient(int id) {

        try {

            String sql = "EXEC ClientSearchID ?";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                CM.setID(rs.getInt(1));
                CM.setCed(rs.getString(2));
                CM.setName(rs.getString(3));
                CM.setLastName(rs.getString(4));
                CM.setAge(rs.getString(5));
                CM.setDirection(rs.getString(6));
                CM.setMail(rs.getString(7));
                CM.setPhone(rs.getString(8));

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return CM;

    }

    public void ShowCategory(int selectedCategory) {
        CC = selectedCategory == 0 ? new Create_client() : new Create_client(selectedCategory);
        //JDesktopPane desktop = (JDesktopPane) clientList.getParent();
        //desktop.add(clientFrame);
        CC.setVisible(true);
    }
}
