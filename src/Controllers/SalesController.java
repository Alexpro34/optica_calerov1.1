package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Models.SalesModel;
import Views.Create_sales;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import Panels.salesPanel;
import javax.swing.JOptionPane;

public class SalesController implements ActionListener {

    salesPanel s;
    Create_sales CS;
    SalesModel sm;
    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public SalesController(Create_sales CS) {
        super();
        this.CS = CS;
        sm = new SalesModel();
    }

    public SalesController(salesPanel s) {
        super();
        this.s = s;
        sm = new SalesModel();
    }

    public SalesModel getSm() {
        return sm;
    }

    public void setSm(SalesModel sm) {
        this.sm = sm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "SearchID":
                searchid();
                break;
            case "SearchCode":
                searchcode();
                break;
            case "GenerateSales":
                generatesales();
                break;
        }
    }

    public void searchid() {
        sm = CS.getSalesData();

        String ID_Client = sm.getID_Client();

        try {
            String sql = " EXEC ClientSearchIDOptometry ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Client);

            rs = ps.executeQuery();

            if (rs.next()) {
                CS.examdateTextField.setText(rs.getString("Exam_Date"));
                CS.medicationTextField.setText(rs.getString("Medication"));
                CS.observationTextArea.setText(rs.getString("Observation"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void searchcode() {
        sm = CS.getSalesData();

        String Code = sm.getCode_Prod();

        try {
            String sql = "EXEC InventorySearchSales ?";
            //String sql = "Select * from dbo.Stock Where ID = ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, Code);

            rs = ps.executeQuery();

            if (rs.next()) {
                CS.brandTextField.setText(rs.getString("Brand"));
                CS.colorTextField.setText(rs.getString("Color"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void generatesales() {
        sm = CS.getSalesData();

        String ID_Client = sm.getID_Client();
        String Exam_Date = sm.getDate_Exam();
        String Medication = sm.getMedication();
        String Observation = sm.getObservation();
        String Code = sm.getCode_Prod();
        String Brand = sm.getBrand();
        String Color = sm.getColor();

        try {
            String sql = "DECLARE @n TINYINT,@nm VARCHAR(500);"
                    + " EXEC SalesInsert ?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                    + "Select @n Codigo,@nm Mensaje";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Client);
            ps.setString(2, Exam_Date);
            ps.setString(3, Medication);
            ps.setString(4, Observation);
            ps.setString(5, Code);
            ps.setString(6, Brand);
            ps.setString(7, Color);

            rs = ps.executeQuery();

            if (rs.next()) {
                String bandera = rs.getString("Codigo");

                if (bandera.equals("0")) {
                    JOptionPane.showMessageDialog(null,rs.getString("Mensaje"));
                } else if (bandera.equals("1")) {
                    JOptionPane.showMessageDialog(null,rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
