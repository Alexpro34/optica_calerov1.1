package Controllers;

import ConnectionSql.ConnectionSQLServer;
import Models.UserModel;
import Panels.userRegistrationPanel;
import Views.Create_user;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class UserController implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    userRegistrationPanel UR;
    Create_user CU;
    UserModel UM;

    public UserController(userRegistrationPanel UR) {
        super();
        this.UR = UR;
        UM = new UserModel();
    }

    public UserController(Create_user CU) {
        super();
        this.CU = CU;
        UM = new UserModel();
    }

    public UserModel getUM() {
        return UM;
    }

    public void setUM(UserModel UM) {
        this.UM = UM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Save":
                UM = CU.getData();

                if (UM.getID_User() == 0) {
                    Insert();
                    CU.dispose();
                } else {
                    Update();
                    CU.dispose();
                }
                break;
            case "Modify":
                ShowCategory(UR.getSelectedCategory());
                break;

            case "Delete":
                Delete(UR.getSelectedCategory());
                break;
        }
    }

    public void Delete(int id) {
        try {

            String sql1 = "EXEC UserSearchID ?";

            ResultSet rs1 = null;
            PreparedStatement ps1 = null;

            ps1 = cc.getConnection().prepareStatement(sql1);

            ps1.setInt(1, id);

            rs1 = ps1.executeQuery();

            if (rs1.next()) {
                String ID_Ced;

                ID_Ced = rs1.getString("Ced");
                String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                        + "EXEC UserDelete ?,@n OUTPUT,@nm OUTPUT;"
                        + "select @n Codigo,@nm Mensaje";

                ResultSet rs = null;
                PreparedStatement ps = null;

                ps = cc.getConnection().prepareStatement(sql);

                ps.setString(1, ID_Ced);

                rs = ps.executeQuery();

                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, rs.getString("Mensaje"));
                }

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void Insert() {

        UM = CU.getData();

        try {
            int ID_User = UM.getID_User();
            String ID_Ced = UM.getID_Ced();
            String User = UM.getUser();
            String Pass = UM.getPassword();
            String Name = UM.getName();
            String LastName = UM.getLastName();
            String Phone = UM.getPhone();
            String Mail = UM.getMail();
            String Department = UM.getDepartment();
            String Municipality = UM.getMunicipality();
            String Role = UM.getRole();
            String Direction = UM.getDirection();
            String RecoveryCode = UM.getRecvoeryCode();

            Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
            String email = Mail;
            Matcher mather = pattern.matcher(email);

            if (ID_Ced.length() == 14) {
                if (!Character.isLetter(ID_Ced.charAt(13))) {
                    JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
                } else {
                    if (mather.find() == true) {

                        String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                                + "EXEC UserInsert ?,?,?,?,?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                                + "select @n Codigo,@nm Mensaje";

                        ResultSet rs = null;
                        PreparedStatement ps = null;

                        ps = cc.getConnection().prepareStatement(sql);

                        ps.setString(1, ID_Ced);
                        ps.setString(2, User);
                        ps.setString(3, Pass);
                        ps.setString(4, Name);
                        ps.setString(5, LastName);
                        ps.setString(6, Phone);
                        ps.setString(7, Mail);
                        ps.setString(8, Department);
                        ps.setString(9, Municipality);
                        ps.setString(10, Role);
                        ps.setString(11, Direction);

                        rs = ps.executeQuery();

                        if (rs.next()) {
                            JOptionPane.showMessageDialog(null, rs.getString("Mensaje"));
                            //STB.LoadUserTable(RP.UserTable);
                            //tiempo.start();
                            //Enviar();
                            //SendWhatsapp();
                            //sendSMS();
                        }
                    } else {
                        //      RP.errorLabel.setVisible(true);
                        JOptionPane.showMessageDialog(null,"El Mail ingresado debe ser valido");
                    }

                }
            } else {
                // RP.errorLabel.setVisible(true);
                JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void Update() {
        UM = CU.getData();

        try {
            int ID_User = UM.getID_User();
            String ID_Ced = UM.getID_Ced();
            String User = UM.getUser();
            String Pass = UM.getPassword();
            String Name = UM.getName();
            String LastName = UM.getLastName();
            String Phone = UM.getPhone();
            String Mail = UM.getMail();
            String Department = UM.getDepartment();
            String Municipality = UM.getMunicipality();
            String Role = UM.getRole();
            String Direction = UM.getDirection();
            String RecoveryCode = UM.getRecvoeryCode();

            Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
            String email = Mail;
            Matcher mather = pattern.matcher(email);

            if (ID_Ced.length() == 14) {
                if (!Character.isLetter(ID_Ced.charAt(13))) {
                    JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
                } else {
                    if (mather.find() == true) {

                        String sql = "DECLARE @n int,@nm varchar(50);"
                                + "EXEC UserUpdate ?,?,?,?,?,?,?,?,?,?,?,@n output,@nm output;"
                                + "SELECT @n Codigo,@nm Mensaje";

                        ResultSet rs = null;
                        PreparedStatement ps = null;

                        ps = cc.getConnection().prepareStatement(sql);

                        ps.setInt(1, ID_User);
                        ps.setString(2, ID_Ced);
                        ps.setString(3, User);
                        ps.setString(4, Name);
                        ps.setString(5, LastName);
                        ps.setString(6, Phone);
                        ps.setString(7, Mail);
                        ps.setString(8, Department);
                        ps.setString(9, Municipality);
                        ps.setString(10, Role);
                        ps.setString(11, Direction);

                        rs = ps.executeQuery();

                        if (rs.next()) {
                            JOptionPane.showMessageDialog(null, rs.getString("Mensaje"));
                            //STB.LoadUserTable(RP.UserTable);
                            //tiempo.start();
                            //Enviar();
                            //SendWhatsapp();
                            //sendSMS();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null,"El Mail ingresado debe ser valido");
                    }

                }
            } else {
                JOptionPane.showMessageDialog(null,"Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public UserModel getUser(int id) {

        try {

            String sql = "EXEC UserSearchID ?";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                UM.setID_User(rs.getInt(1));
                UM.setID_Ced(rs.getString(2));
                UM.setUser(rs.getString(3));
                UM.setPassword(rs.getString(4));
                UM.setName(rs.getString(5));
                UM.setLastName(rs.getString(6));
                UM.setPhone(rs.getString(7));
                UM.setMail(rs.getString(8));
                UM.setDepartment(rs.getString(9));
                UM.setMunicipality(rs.getString(10));
                UM.setRole(rs.getString(11));
                UM.setDirection(rs.getString(12));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return UM;

    }

    public void ShowCategory(int selectedCategory) {
        CU = selectedCategory == 0 ? new Create_user() : new Create_user(selectedCategory);
        //JDesktopPane desktop = (JDesktopPane) clientList.getParent();
        //desktop.add(clientFrame);
        CU.setVisible(true);
    }
}
