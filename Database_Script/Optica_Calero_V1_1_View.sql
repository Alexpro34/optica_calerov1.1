/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	VIEW TABLES <<<<<<<<<<<<<
*/
--=======================================================================================--
--=======================================VIEW USERS=====================================---
DROP VIEW IF EXISTS ViewUsers
GO

CREATE VIEW ViewUsers
AS
	SELECT U.ID_User,p.Ced,u.[User],u.[Password],p.[Name],p.LastName,p.Phone,p.Mail,u.Departments,u.Municipality,u.[Role],p.Direction,u.RecoveryCode
	FROM dbo.Person p
	INNER JOIN dbo.Users U ON u.ID_Person = p.ID_Person
GO
--=======================================================================================--
--=======================================VIEW CLIENTS=====================================---
DROP VIEW IF EXISTS ViewClients
GO

CREATE VIEW ViewClients
AS
	SELECT u.ID_Client,p.Ced,p.[Name],p.LastName,u.Age,p.Direction,p.Mail,p.Phone
	FROM dbo.Person p
	INNER JOIN dbo.Client U ON u.ID_Person = p.ID_Person
GO
--=======================================================================================--
--=============VIEW FOR SALES RECORD TABLE====================--
DROP VIEW IF EXISTS SalesRecord
GO

CREATE VIEW SalesRecord
AS
	SELECT S.ID_Sales,C.ID_Client,P.Name,P.LastName,S.Brand,S.Color,I.Price FROM dbo.Client C
	INNER JOIN dbo.Sales S ON S.ID_Client = C.ID_Client
	INNER JOIN dbo.Stock I ON I.ID = S.ID
	INNER JOIN dbo.Person P ON P.ID_Person = C.ID_Person
GO
--=======================================================================================--