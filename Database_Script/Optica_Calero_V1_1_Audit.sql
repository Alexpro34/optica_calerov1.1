------CREACION DE LA TABLA INVENTORY------------------------------------------------------
DROP TABLE IF EXISTS dbo.StockAudit
GO
CREATE TABLE dbo.StockAudit(
	[ID] TINYINT,
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	[Quantity] VARCHAR(20),
	[Price] VARCHAR(20),
	[DateTime] DATETIME,
	TypeMove VARCHAR(50),
	ServerName VARCHAR(50),
	ServiceName VARCHAR(50)
)
GO

CREATE TRIGGER dbo.StockInsert
ON dbo.Stock
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO dbo.StockAudit
	SELECT *,GETDATE(),'Insert',@@SERVERNAME,@@SERVICENAME FROM inserted;
END

SELECT * FROM dbo.Stock
SELECT * FROM dbo.StockAudit
-------------------------------------------------------------------------------------
DROP TABLE IF EXISTS dbo.SalesAudit
GO

CREATE TABLE dbo.SalesAudit(
	[ID_Sales] TINYINT,
	[ID_Client] TINYINT,
	[Exam_Date] DATE,
	[Medication] VARCHAR(20),
	[Observation] VARCHAR(500),
	[ID] TINYINT,
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	[DateTime] DATETIME,
	TypeMove VARCHAR(50),
	ServerName VARCHAR(50),
	ServiceName VARCHAR(50)
)
GO


CREATE TRIGGER dbo.SalesInsertAudit
ON dbo.Sales
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO dbo.SalesAudit
	SELECT *,GETDATE(),'Insert',@@SERVERNAME,@@SERVICENAME FROM inserted;
END