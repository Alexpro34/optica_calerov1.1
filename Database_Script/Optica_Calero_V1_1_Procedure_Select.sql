/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT II
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	SELECT STORED PROCEDURES <<<<<<<<<<<<<
*/
--=======================================================================================--
--==========================PROCEDURE TO VALIDATE LOGIN==================================--
DROP PROCEDURE IF EXISTS LoginProgram
GO

CREATE PROCEDURE LoginProgram
	@User VARCHAR(40),
	@Password VARCHAR(40),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
		IF NOT EXISTS(SELECT * FROM Users WHERE [User] = @User AND [Password] = HASHBYTES('SHA2_256',@Password))
			BEGIN
				SET @RETURN = 1
				SET @RETURN_MESSAGE = 'User Or Password Incorrectly'
			END
			ELSE 
				BEGIN
					SET @RETURN = 0
					SET @RETURN_MESSAGE = 'Welcome'
				END
GO
--=======================================================================================--
--==========================PROCEDURE TO VALIDATE ROLE==================================--
DROP PROCEDURE IF EXISTS LoginFull
GO

CREATE PROCEDURE LoginFull
	@User VARCHAR(40)
AS
	BEGIN
		IF EXISTS(SELECT [Role] FROM Users WHERE [User]=@User)
		BEGIN
			SELECT [Role] FROM Users WHERE [User]=@User
		END	
	END
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING FOR ADMIN OR EMPLOYEE TYPE USER====================--
DROP PROCEDURE IF EXISTS UserSearchID
GO

CREATE PROCEDURE UserSearchID
	@ID_User VARCHAR(40) = NULL
AS
	SELECT U.ID_User,p.Ced,u.[User],u.[Password],p.[Name],p.LastName,p.Phone,p.Mail,u.Departments,u.Municipality,u.[Role],p.Direction,u.RecoveryCode
	FROM dbo.Person p
	INNER JOIN dbo.Users U ON u.ID_Person = p.ID_Person 
	WHERE U.ID_User = @ID_User
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING FOR ADMIN OR EMPLOYEE TYPE USER====================--
DROP PROCEDURE IF EXISTS UserSearch
GO

CREATE PROCEDURE UserSearch
	@ID_Ced VARCHAR(40) = NULL
AS
	SELECT U.ID_User,p.Ced,u.[User],u.[Password],p.[Name],p.LastName,p.Phone,p.Mail,u.Departments,u.Municipality,u.[Role],p.Direction,u.RecoveryCode
	FROM dbo.Person p
	INNER JOIN dbo.Users U ON u.ID_Person = p.ID_Person 
	WHERE p.Ced LIKE @ID_Ced+'%' OR @ID_Ced IS NULL
GO
--=======================================================================================--
--=========================PROCEDURE FOR LOOKING CLIENT==================================--
DROP PROCEDURE IF EXISTS ClientSearchID
GO

CREATE PROCEDURE ClientSearchID
	@ID_Client VARCHAR(40) =  NULL
AS
	SELECT u.ID_Client,p.Ced,p.[Name],p.LastName,u.Age,p.Direction,p.Mail,p.Phone
	FROM dbo.Person p
	INNER JOIN dbo.Client U ON u.ID_Person = p.ID_Person 
	WHERE u.ID_Client = @ID_Client
GO
--=======================================================================================--
--=========================PROCEDURE FOR LOOKING CLIENT==================================--
DROP PROCEDURE IF EXISTS ClientSearch
GO

CREATE PROCEDURE ClientSearch
	@ID_Ced VARCHAR(40) =  NULL
AS
	SELECT u.ID_Client,p.Ced,p.[Name],p.LastName,u.Age,p.Direction,p.Mail,p.Phone
	FROM dbo.Person p
	INNER JOIN dbo.Client U ON u.ID_Person = p.ID_Person 
	WHERE p.Ced LIKE @ID_Ced+'%' OR @ID_Ced IS NULL
GO
--=======================================================================================--
--==========================PROCEDURE FOR LOOKING INVENTORY==============================--
DROP PROCEDURE IF EXISTS InventorySearch
GO

CREATE PROCEDURE InventorySearch
@CODE VARCHAR(40)
AS
    SELECT * FROM dbo.Stock WHERE [CODE] LIKE @CODE+'%'
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING FOR CLIENT IN OPTOMETRY============================--
DROP PROCEDURE IF EXISTS ClientSearchIDOptometry
GO

CREATE PROCEDURE ClientSearchIDOptometry
	@ID_Client VARCHAR(40)
AS
	SELECT o.Exam_Date,o.Medication,o.Observation FROM dbo.Optometry o WHERE [ID_Client] = @ID_Client
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING ITEM TO INVENTORY====================--
DROP PROCEDURE IF EXISTS InventorySearchSales
GO

CREATE PROCEDURE InventorySearchSales
	@CODE VARCHAR(40)
AS
    SELECT i.Brand,i.Color FROM dbo.Stock i WHERE [CODE] = @CODE
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING ITEM TO INVENTORY====================--
DROP PROCEDURE IF EXISTS SalesSearch
GO

CREATE PROCEDURE SalesSearch
	@ID_Client VARCHAR(40)
AS
    SELECT * FROM dbo.Sales WHERE ID_Client LIKE @ID_Client + '%'
GO
--=======================================================================================--
--==============PROCEDURE FOR LOOKING ITEM TO INVENTORY====================--
DROP PROCEDURE IF EXISTS OptometrySearch
GO

CREATE PROCEDURE OptometrySearch
	@ID_Client VARCHAR(50) = NULL
AS
    SELECT * FROM dbo.Optometry WHERE ID_Client LIKE @ID_Client+'%'
GO

EXEC OptometrySearch 1
--=======================================================================================--
--==============PROCEDURE FOR SHOW SALES RECORD====================--
DROP PROCEDURE IF EXISTS ShowSalesRecord
GO

CREATE PROCEDURE ShowSalesRecord
	@ID_Sales INT = NULL
AS
	SELECT * FROM SalesRecord WHERE ID_Sales = @ID_Sales OR @ID_Sales IS NULL
GO
--=======================================================================================--

EXEC ShowSalesRecord 1