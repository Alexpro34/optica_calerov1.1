/*
	DATABASE OPTICA_CALERO_V1.1
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT II
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/
----=======================================================================================--
----CREATE DATABASE--
--DROP DATABASE IF EXISTS Optica_Calero_V1_1;
--GO

CREATE DATABASE Optica_Calero_V1_1;
GO
------USAMOS OPTICA CALERO-----------------------------------------------------------------------
USE Optica_Calero_V1_1;
-------------------------------------------------------------------------------------------------
------CREATE TABLE DEPARTMENTS------------------------------------------------------
DROP TABLE IF EXISTS [dbo].[departments]
GO
 CREATE TABLE [dbo].[departments](
 [ID] INT IDENTITY(1,1) PRIMARY KEY,
 [Name_Departments] varchar (200) NOT NULL,
 )
 GO

 INSERT INTO dbo.departments
VALUES ('BOACO'),
 ('CARAZO'),
 ('CHINANDEGA'),
 ('CHONTALES'),
 ('COSTA CARIBE NORTE'),
 ('COSTA CARIBE SUR'),
 ('ESTELI'),
 ('GRANADA'),
 ('JINOTEGA'),
 ('LEON'),
 ('MADRIZ'),
 ('MANAGUA'),
 ('MASAYA'),
 ('MATAGALPA'),
 ('NUEVA SEGOVIA'),                                                                       
 ('RIO SAN JUAN'),
 ('RIVAS');
 -------------------------------------------------------------------------------------------------
------CREATE TABLE MUNICIPALITY------------------------------------------------------
 DROP TABLE IF EXISTS dbo.Municipality 
 GO

 create table [dbo].[Municipality](
 [ID_M] int IDENTITY(1,1),
 [Name_Municipality] varchar (255) NOT NULL,
 ID INT,
 FOREIGN KEY (ID) REFERENCES dbo.Departments(ID)
 )
 go

 INSERT INTO [dbo].[Municipality]
VALUES
 ('BOACO',1),
 ('CAMOAPA',1),
 ('SAN JOSE DE LOS REMATES',1),
 ('SAN LORENZO',1),
 ('SANTA LUCIA',1),
 ('TEUSTEPE',1),

 ('DIRIAMBA',2),
 ('DOLORES',2),
 ('EL ROSARIO',2),
 ('JINOTEPE',2),
 ('LA CONQUISTA',2),
 ('SAN MARCOS',2),
 ('SANTA TEREZA',2),

 ('CHICHIGALPA',3),
 ('CHINANDEGA',3),
 ('CINCO PINOS',3),
 ('CORINTO',3),
 ('CORINTO',3),
 ('EL REALEJO',3),
 ('EL VIEJO',3),
 ('POSOLTEGA',3),
 
 ('ACOYAPA',4),
 ('COMALAPA',4),
 ('CUAPA',4),
 ('EL CORAL',4),
 ('JUIGALPA',4),
 ('LA LIBERTAD',4),
 ('SAN PEDRO',4),
 ('SANTO DOMINGO',4),
 ('SANTO TOMAS',4),
 ('VILLA SANDINO',4),
 
 ('BONANZA',5),
 ('MULUKUKU',5),
 ('PRINZAPOLKA',5),
 ('PUERTO CABEZAS',5),
 ('ROSITA',5),
 ('SIUNA',5),
 ('WASLALA',5),
 ('WASPAN',5),
 
 ('BLUFIELD',6),
 ('DESEMBOCADURA DE RIO GRANDE',6),
 ('EL AYOTE',6),
 ('EL RAMA',6),
 ('EL TORTUGUERO',6),
 ('ISLA DEL MAIZ',6),
 ('KUKRA HILL',6),
 ('LA CRUZ DEL RIO GRANDE ',6),
 ('LAGUNA DE PERLAS',6),
 ('EL MUELLE DE LOS BUEYES',6),
 ('NUEVA GUINEA',6),
 ('PAIWAS',6),


 ('CONDEGA',7),
 ('ESTELI',7),
 ('LA TRINIDAD',7),
 ('PUEBLO NUEVO',7),
 ('SAN JUAN DE LIMAY',7),
 ('SAN NICOLAS',7),

 ('DIRIA',8),
 ('DIRIOMO',8),
 ('GRANADA',8),
 ('NANDAIME',8),

 ('EL CUA',9),
 ('JINOTEGA',9),
 ('LA CONCORDIA',9),
 ('SAN JOSE DE BOCAI',9),
 ('SAN RAFAEL DEL NORTE',9),
 ('SAN SEBASTIAN DE YALI',9),
 ('SANTA MARIA DE PANTASMA',9),
 ('WIWILI DE JINOTEGA',9),

 ('ACHUAPA',10),
 ('EL JICARAL',10),
 ('EL SAUCE',10),
 ('LA PAZ CENTRO',10),
 ('LA REYNAGA',10),
 ('LEON',10),
 ('NAGAROTE',10),
 ('QUEZALGUAQUE',10),
 ('SANTA ROSA DEL PE�ON',10),
 ('TELICA',10),

 ('LAS SABANAS',11),
 ('PALACAGUINAS',11),
 ('SAN JOSE DE CUSMA',11),
 ('SAN JUAN DE RIO COCO',11),
 ('SAN LUCAS',11),
 ('SOMOTO',11),
 ('TELPANECA',11),
 ('TOTOGALPA',11),
 ('YALAGUINA',11),
 
 ('CIUDAD SANDINO',12),
 ('EL CRUCERO',12),
 ('MANAGUA',12),
 ('MATEARE',12),
 ('SAN FRANCISCO LIBRE',12),
 ('SAN RAFAEL DEL SUR',12),
 ('TICUANTEPE',12),
 ('TIPITAPA',12),
 ('VILLA EL CARMEN',12),
 
 ('CATARINA',13),
 ('LA CONCEPCION',13),
 ('MASATEPE',13),
 ('MASAYA',13),
 ('NANDASMO',13),
 ('NINDIRI',13),
 ('NIQUINOHOMO',13),
 ('SAN JUAN DE ORIENTE',13),
 ('TISMA',13),

 ('CIUDAD DARIO',14),
 ('EL TUMA-LA DALIA',14),
 ('ESQUIPULAS',14),
 ('MATAGALPA',14),
 ('MATIGUAS',14),
 ('MUY MUY',14),
 ('RANCHO GRANDE',14),
 ('RIO BLANCO',14),
 ('SAN DIONISIO',14),
 ('SAN ISIDRO',14),
 ('SAN RAMON',14),
 ('SEBACO',14),
 ('TERRABONA',14),

 ('CIUDAD ANTIGUA',15),
 ('DIPILTO',15),
 ('EL JICARO',15),
 ('JALAPA',15),
 ('MACUELIZO',15),
 ('MOZONTE',15),
 ('MURRA',15),
 ('OCOTAL',15),
 ('QUILALI',15),
 ('SAN FERNANDO',15),
 ('SANTA MARIA',15),
 ('WIWILI',15),

 ('EL ALMENDRO',16),
 ('EL CASTILLO',16),
 ('MORRITO',16),
 ('SAN CARLOS',16),
 ('SAN JUAN DEL NORTE',16),
 ('SAN MIGUELITO',16),

 ('ALTAGRACIA',17),
 ('BELEN',17),
 ('BUENOS AIRES',17),
 ('CARDENAS',17),
 ('MOYOGALPA',17),
 ('POTOSI',17),
 ('RIVAS',17),
 ('SAN JORGE',17),
 ('SAN JUAN DEL SUR',17),
 ('TOLA',17);
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA PERSONAS------------------------------------------------------
DROP TABLE IF EXISTS dbo.Person
GO

CREATE TABLE dbo.Person(
	[ID_Person] TINYINT IDENTITY(1,1),
	[Name] VARCHAR(20),
	[LastName] VARCHAR(20),
	[Phone] VARCHAR(20) NOT NULL,
	[Mail] VARCHAR(50) NOT NULL,
	[Direction] VARCHAR(30) NOT NULL,
	[Ced] VARCHAR(50) NOT NULL,
	PRIMARY KEY([ID_Person]),
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA ADMIN Y EMPLEADO------------------------------------------------------
DROP TABLE IF EXISTS dbo.Users;
GO
CREATE TABLE dbo.Users(
	[ID_User] TINYINT IDENTITY(1,1) NOT NULL,
	[User] VARCHAR(20) NOT NULL,
	[Password] VARBINARY(100) NOT NULL DEFAULT ( HASHBYTES('SHA2_256','0x')),	
	[Role] VARCHAR(20) NOT NULL,
	[RecoveryCode] INT,
	[Departments] VARCHAR(50),
	[Municipality] VARCHAR(50),
	[ID_Person] TINYINT,
	PRIMARY KEY ([ID_User]),
	FOREIGN KEY ([ID_Person]) REFERENCES dbo.Person ([ID_Person])
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA CLIENT------------------------------------------------------
DROP TABLE IF EXISTS dbo.Client;
GO
CREATE TABLE dbo.Client(
	[ID_Client] TINYINT IDENTITY(1,1) NOT NULL,
	[Age] TINYINT NOT NULL,
	[Ced] VARCHAR(50) NOT NULL,
	[ID_Person] TINYINT,
	PRIMARY KEY([ID_Client]),
	FOREIGN KEY ([ID_Person]) REFERENCES dbo.Person([ID_Person])
)
GO

-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA INVENTORY------------------------------------------------------
DROP TABLE IF EXISTS dbo.Stock
GO
CREATE TABLE dbo.Stock(
	[ID] TINYINT IDENTITY(1,1),
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	[Quantity] VARCHAR(20),
	[Price] VARCHAR(20),
	PRIMARY KEY ([ID])
)
GO
---------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA DISTANCE------------------------------------------------------
DROP TABLE IF EXISTS dbo.Distance
GO

CREATE TABLE dbo.Distance(
	ID_Distance INT IDENTITY(1,1),
	Name_Distance VARCHAR(50)
)
GO

INSERT INTO dbo.Distance
VALUES('Only Near'),('Only Far'),('Both')
GO
---------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA MEDICATION------------------------------------------------------
DROP TABLE IF EXISTS dbo.Medication
GO

CREATE TABLE dbo.Medication(
	ID_Medication INT IDENTITY(1,1),
	Name_Medication VARCHAR(50) NOT NULL
)
GO

INSERT INTO dbo.Medication
VALUES('Monofocal'),('Bifocal'),('Progressive')
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA OPTOMETRY------------------------------------------------------
DROP TABLE IF EXISTS dbo.Optometry
GO
CREATE TABLE dbo.Optometry(
	[ID_Exam] INT IDENTITY(1,1) PRIMARY KEY,
	[ID_Client] TINYINT,
	[Exam_Date] DATE,
	[Eyes_Right] VARCHAR(20),
	[Eyes_Left] VARCHAR(20),
	[Lenses_Type] VARCHAR(20),
	[Observation] VARCHAR(500),
	[Distance] VARCHAR(200),
	[Medication] VARCHAR(200),
	FOREIGN KEY ([ID_Client]) REFERENCES dbo.Client([ID_Client]),
)
GO

---------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA SALES------------------------------------------------------
DROP TABLE IF EXISTS dbo.Sales
GO

CREATE TABLE dbo.Sales(
	[ID_Sales] TINYINT IDENTITY(1,1),
	[ID_Client] TINYINT,
	[Exam_Date] DATE,
	[Medication] VARCHAR(20),
	[Observation] VARCHAR(500),
	[ID] TINYINT,
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	PRIMARY KEY ([ID_Sales]),
	FOREIGN KEY ([ID_Client]) REFERENCES dbo.Client([ID_Client]),
	FOREIGN KEY ([ID]) REFERENCES dbo.Stock([ID])
)
GO
------------------------------------------------------------------------
SELECT * FROM dbo.Sales